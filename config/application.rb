# Configure the main application
module SinatraAudition
  module Configuration
    module Application

      # Configure after registering this module
      def self.registered(application)

        # Main configuration
        application.configure do |config|

          # Use Thin server by default
          config.set :server, :thin

        end
      end

    end
  end
end
