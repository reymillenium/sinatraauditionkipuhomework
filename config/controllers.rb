# Configure Controllers
module SinatraAudition
  module Configuration
    module Controllers

      # Configure after registering this module 
      def self.registered(controller)

        # Main configuration
        controller.configure do |config|

          # Configure views
          config.set :views, './views'

          # Custom exception handling
          config.set :show_exceptions, false

        end

        # Development configuration
        controller.configure :development do |config|

          # Reload controller code when it changes
          config.register Sinatra::Reloader

        end
      end

    end
  end
end
