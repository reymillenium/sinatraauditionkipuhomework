require './config/application'
require './controllers/application_controller'

# Define main application (Starting point)
module SinatraAudition
  class Application < Sinatra::Base

    # Configure the application
    register Configuration::Application

    # Delegate to the application controller
    use ApplicationController

  end
end
