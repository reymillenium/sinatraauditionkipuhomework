require 'rubygems'
require 'bundler'
 
Bundler.require
require './application'

# Run sinatra application entry point
run SinatraAudition::Application
