require 'sinatra/base'
require 'sinatra/reloader'
require 'sinatra/json'

require './config/controllers'

# Home page controller
module SinatraAudition
  class ApplicationController < Sinatra::Base

    # Configure controllers
    register Configuration::Controllers

    # Define some helpers
    helpers do

      # Get error
      def get_error
        env['sinatra.error']
      end

      # Get error message
      def error_message
        get_error.message
      end

      # Get error details
      def error_details
        get_error.backtrace
      end

    end

    # Handle not found routes
    not_found do
      erb :not_found
    end

    # Handle errors
    error do
      erb :error, locals: {
        error_message: error_message,
        error_datails: error_details
      }
    end

    # Application root
    get '/' do
      redirect to('/home')
    end

    # Home route
    get '/home' do
     erb :home
    end

    # Code route
    get '/code' do
      json code: 'the code you wrote'
    end

  end
end
